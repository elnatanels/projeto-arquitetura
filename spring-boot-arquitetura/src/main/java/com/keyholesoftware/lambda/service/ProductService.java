package com.keyholesoftware.lambda.service;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.keyholesoftware.lambda.model.Product;
import com.keyholesoftware.lambda.repository.ProductRepository;




@Service
@Validated
public class ProductService {

	@Autowired
	private ProductRepository repository;

	public Product buscarUsuarioPorId(String id) {
		return repository.findOne(id);
	}

	public Iterable<Product> listarTodosUsuarios() {
		return repository.findAll();
	}

	@Transactional
	public Product inserirUsuario(Product usuario) {
		return repository.save(usuario);
	}

	public ProductRepository getRepository() {
		return repository;
	}

	public void setRepository(ProductRepository repository) {
		this.repository = repository;
	}
}
