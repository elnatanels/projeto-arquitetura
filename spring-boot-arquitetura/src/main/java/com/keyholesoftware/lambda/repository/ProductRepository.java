package com.keyholesoftware.lambda.repository;


import org.springframework.data.repository.CrudRepository;

import com.keyholesoftware.lambda.model.Product;


public interface ProductRepository extends CrudRepository<Product, String> {

}
